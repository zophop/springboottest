package com.test.springboot.springboottest.springboottest.controller;

import com.test.springboot.springboottest.springboottest.exception.ResourceNotFound;
import com.test.springboot.springboottest.springboottest.model.Vehicles;
import com.test.springboot.springboottest.springboottest.repository.VehiclesRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class VehiclesController{

    private static final Logger logger = LogManager.getLogger();

    @Autowired
    private VehiclesRepository vehiclesRepository;

    @GetMapping("/healthcheck")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        return String.format("API is Live\n");
    }

    //    create user
    @PostMapping("/vehicles")
    public Vehicles createUser(@RequestBody Vehicles vehicles){
        logger.warn("User Created Successfully!");
        return vehiclesRepository.save(vehicles);
    }

    //    Get user-vehicles
    @GetMapping("/vehicles/{id}") // use @pathVariable Annotation to bind url value
    public ResponseEntity<Vehicles> getVehiclesById(@PathVariable(name = "id")long id) throws ResourceNotFound {
        Vehicles vehicles = vehiclesRepository.findById(id).orElseThrow(() -> new ResourceNotFound("GetMapping : No Such Vehicle data : "+id));
        logger.warn(vehicles);
        return ResponseEntity.ok().body(vehicles);
    }


    //    update-vehicles
    @PutMapping("/vehicles/{id}")
    public ResponseEntity<Vehicles> updateVehiclesById(@PathVariable(name = "id")long id, @RequestBody Vehicles vehiclesDetails) throws ResourceNotFound {
        Vehicles vehicles = vehiclesRepository.findById(id).orElseThrow(() -> new ResourceNotFound("PutMapping : No Such Vehicle data : "+id));
        vehicles.setId(vehiclesDetails.getId());
        vehicles.setName(vehiclesDetails.getName());
        vehiclesRepository.save(vehicles);
        logger.warn(vehicles);
        return ResponseEntity.ok().body(vehicles);
    }


    //    delete vehicles
    @DeleteMapping("/vehicles/{id}")
    public ResponseEntity deleteVehicles(@PathVariable(name = "id")long id) throws ResourceNotFound {
        vehiclesRepository.findById(id).orElseThrow(() -> new ResourceNotFound("DeleteMapping : No Such Vehicle data : "+id));
        vehiclesRepository.deleteById(id);
        logger.warn("DeleteMapping : Vehicle"+id+" deleted");
        return ResponseEntity.ok().body(String.format("deletion Successfully"));
    }

}