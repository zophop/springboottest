package com.test.springboot.springboottest.springboottest.repository;

import com.test.springboot.springboottest.springboottest.model.Vehicles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehiclesRepository extends JpaRepository<Vehicles,Long> {
}
